﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GiteeOpenSdk.WebHook.Models
{
    /// <summary>
    /// Gitee WebHook Note 返回模型
    /// </summary>
    public class WebHookNoteModel : WebHookBaseModel
    {
        /// <summary>
        /// 评论的动作。eg：comment
        /// </summary>
        [JsonProperty("action")] 
        public string Action { get; set; }
        /// <summary>
        /// 评论的数据信息。
        /// </summary>
        [JsonProperty("comment")]
        public Commit Comment { get; set; }
        /// <summary>
        /// 评论所在仓库的信息。
        /// </summary>
        [JsonProperty("repository")]
        public Repository Repository { get; set; }
        /// <summary>
        /// 评论所在仓库的信息。
        /// </summary>
        [JsonProperty("project")]
        public Project Project { get; set; }
        /// <summary>
        /// 评论的作者信息。
        /// </summary>
        [JsonProperty("author")]
        public User Author { get; set; }
        /// <summary>
        /// 评论的作者信息。
        /// </summary>
        [JsonProperty("sender")]
        public User Sender { get; set; }
        /// <summary>
        /// 这条评论在 Gitee 上的 url。eg：https://gitee.com/oschina/git-osc#note_1
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 评论内容。eg：好的东西应该开源...
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }
        /// <summary>
        /// 被评论的目标类型。eg：Issue
        /// </summary>
        [JsonProperty("noteable_type")]
        public string NoteableType { get; set; }
        /// <summary>
        /// 被评论的目标 id。
        /// </summary>
        [JsonProperty("noteable_id")]
        public int NoteableId { get; set; }
        /// <summary>
        /// 被评论的 Issue 信息。
        /// </summary>
        [JsonProperty("issue")]
        public Issue Issue { get; set; }
        /// <summary>
        /// 被评论的 PR 信息。
        /// </summary>
        [JsonProperty("pull_request")]
        public PullRequest PullRequest { get; set; }
        /// <summary>
        /// 被评论的目标标题。eg：这是一个 PR 标题
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 被评论的目标标识。eg：IG6E9
        /// </summary>
        [JsonProperty("per_iid")]
        public string PerIid { get; set; }
        /// <summary>
        /// 被评论的 commit 提交中的简短 sha。eg：51b1acb
        /// </summary>
        [JsonProperty("short_commit_id")]
        public string ShortCommitId { get; set; }
        /// <summary>
        /// 被评论的目标所在的企业信息。
        /// </summary>
        [JsonProperty("enterprise")]
        public Enterprise Enterprise { get; set; }
        
    }
}
