﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class User
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 与上面的 username 一致。
        /// </summary>
        [JsonProperty("login")]
        public string Login { get; set; }
        /// <summary>
        /// 用户头像 url。eg：https://gitee.com/assets/favicon.ico
        /// </summary>
        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 与上面的 url 一致。
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// 用户类型，目前固定为 User。
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 是不是管理员。
        /// </summary>
        [JsonProperty("site_admin")]
        public bool SiteAdmin { get; set; }

        /// <summary>
        /// 用户的昵称。eg：红薯
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 用户的邮箱。eg：git@oschina.cn
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 用户的 Gitee 个人空间地址。eg：gitee
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 与上面的 username 一致。
        /// </summary>
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        /// <summary>
        /// 用户的 Gitee 个人主页 url。eg：https://gitee.com/gitee
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// git commit 中的时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("time")]
        public string Time { get; set; }
        /// <summary>
        /// 用户备注名。eg：Ruby 大神
        /// </summary>
        [JsonProperty("remark")]
        public string Remark { get; set; }
    }
}
