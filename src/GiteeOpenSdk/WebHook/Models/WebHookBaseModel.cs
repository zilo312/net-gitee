﻿using Newtonsoft.Json;

namespace GiteeOpenSdk.WebHook.Models
{
    /// <summary>
    /// Gitee WebHook 公共返回参数
    /// </summary>
    public class WebHookBaseModel
    {
        [JsonProperty("hook_name")]
        public string HookName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("hook_id")]
        public string HookId { get; set; }

        [JsonProperty("hook_url")]
        public string HookUrl { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("sign")]
        public string Sign { get; set; }
    }
}