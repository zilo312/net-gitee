using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 创建Pull Request
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoPullsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 必填。Pull Request 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// 必填。Pull Request 提交目标分支的名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("base")]
        public string Base { get; set; }
        /// <summary>
        /// 可选。Pull Request 内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 可选。里程碑序号(id)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone_number")]
        public int? MilestoneNumber { get; set; }
        /// <summary>
        /// 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue")]
        public string Issue { get; set; }
        /// <summary>
        /// 可选。审查人员username，可多个，半角逗号分隔，如：(username1,username2), 注意: 当仓库代码审查设置中已设置【指派审查人员】则此选项无效
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignees")]
        public string Assignees { get; set; }
        /// <summary>
        /// 可选。测试人员username，可多个，半角逗号分隔，如：(username1,username2), 注意: 当仓库代码审查设置中已设置【指派测试人员】则此选项无效
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("testers")]
        public string Testers { get; set; }
        /// <summary>
        /// 可选。最少审查人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignees_number")]
        public int? AssigneesNumber { get; set; }
        /// <summary>
        /// 可选。最少测试人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("testers_number")]
        public int? TestersNumber { get; set; }
        /// <summary>
        /// 可选。合并PR后是否删除源分支，默认false（不删除）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prune_source_branch")]
        public bool? PruneSourceBranch { get; set; }
        /// <summary>
        /// 可选，合并后是否关闭关联的 Issue，默认根据仓库配置设置
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("close_related_issue")]
        public bool? CloseRelatedIssue { get; set; }

    }
}