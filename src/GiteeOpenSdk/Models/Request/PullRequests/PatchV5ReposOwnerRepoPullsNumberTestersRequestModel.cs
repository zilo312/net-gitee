using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 重置 Pull Request 测试 的状态
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoPullsNumberTestersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 第几个PR，即本仓库PR的序数
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 是否重置所有测试人，默认：false，只对管理员生效
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("reset_all")]
        public bool? ResetAll { get; set; }

    }
}