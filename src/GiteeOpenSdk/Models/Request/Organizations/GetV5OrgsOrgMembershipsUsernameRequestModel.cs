using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 获取授权用户所属组织的一个成员
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5OrgsOrgMembershipsUsernameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }

    }
}