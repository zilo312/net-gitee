using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 更新授权用户在一个组织的成员资料
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5UserMembershipsOrgsOrgRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 在组织中的备注信息
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("remark")]
        public string Remark { get; set; }

    }
}