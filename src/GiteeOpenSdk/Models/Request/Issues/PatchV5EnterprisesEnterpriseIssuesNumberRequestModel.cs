using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 更新企业的某个Issue
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5EnterprisesEnterpriseIssuesNumberRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// Issue 编号(区分大小写，无需添加 # 号)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// Issue标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// Issue 状态，open（开启的）、progressing（进行中）、closed（关闭的）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// Issue描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// Issue负责人的个人空间地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// Issue协助者的个人空间地址, 以 , 分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("collaborators")]
        public string Collaborators { get; set; }
        /// <summary>
        /// 里程碑序号
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone")]
        public int? Milestone { get; set; }
        /// <summary>
        /// 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program")]
        public string Program { get; set; }
        /// <summary>
        /// 是否是私有issue(默认为false)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("security_hole")]
        public bool? SecurityHole { get; set; }
        /// <summary>
        /// 分支名称，传空串表示取消关联分支
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("branch")]
        public string Branch { get; set; }

    }
}