using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 更新Issue某条评论
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoIssuesCommentsIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 评论的ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// The contents of the comment.
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }

    }
}