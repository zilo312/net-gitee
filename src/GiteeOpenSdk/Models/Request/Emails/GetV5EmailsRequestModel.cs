using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Emails
{
    /// <summary>
    /// 获取授权用户的全部邮箱
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EmailsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

    }
}