using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Milestones
{
    /// <summary>
    /// 更新仓库里程碑
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoMilestonesNumberRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 里程碑序号(id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 里程碑标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 里程碑状态: open, closed, all。默认: open
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 里程碑具体描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 里程碑的截止日期
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("due_on")]
        public string DueOn { get; set; }

    }
}