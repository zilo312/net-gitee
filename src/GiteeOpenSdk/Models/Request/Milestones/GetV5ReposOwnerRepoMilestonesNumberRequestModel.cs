using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Milestones
{
    /// <summary>
    /// 获取仓库单个里程碑
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoMilestonesNumberRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 里程碑序号(id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }

    }
}