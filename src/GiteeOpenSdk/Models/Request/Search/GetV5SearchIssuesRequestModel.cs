using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Search
{
    /// <summary>
    /// 搜索 Issues
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5SearchIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("q")]
        public string Q { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 筛选指定仓库 (path, e.g. oschina/git-osc) 的 issues
        /// </summary>
        [RequestType("query")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 筛选指定语言的 issues
        /// </summary>
        [RequestType("query")]
        [JsonProperty("language")]
        public string Language { get; set; }
        /// <summary>
        /// 筛选指定标签的 issues
        /// </summary>
        [RequestType("query")]
        [JsonProperty("label")]
        public string Label { get; set; }
        /// <summary>
        /// 筛选指定状态的 issues, open(开启)、closed(完成)、rejected(拒绝)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 筛选指定创建者 (username/login) 的 issues
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author")]
        public string Author { get; set; }
        /// <summary>
        /// 筛选指定负责人 (username/login) 的 issues
        /// </summary>
        [RequestType("query")]
        [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// 排序字段，created_at(创建时间)、last_push_at(更新时间)、notes_count(评论数)，默认为最佳匹配
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序顺序: desc(default)、asc
        /// </summary>
        [RequestType("query")]
        [JsonProperty("order")]
        public string Order { get; set; }

    }
}