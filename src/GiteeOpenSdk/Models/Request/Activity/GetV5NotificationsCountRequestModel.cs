using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 获取授权用户的通知数
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5NotificationsCountRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 是否只获取未读消息，默认：否
        /// </summary>
        [RequestType("query")]
        [JsonProperty("unread")]
        public bool? Unread { get; set; }

    }
}