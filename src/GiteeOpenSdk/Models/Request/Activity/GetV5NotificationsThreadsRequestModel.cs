using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 列出授权用户的所有通知
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5NotificationsThreadsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 是否只获取未读消息，默认：否
        /// </summary>
        [RequestType("query")]
        [JsonProperty("unread")]
        public bool? Unread { get; set; }
        /// <summary>
        /// 是否只获取自己直接参与的消息，默认：否
        /// </summary>
        [RequestType("query")]
        [JsonProperty("participating")]
        public bool? Participating { get; set; }
        /// <summary>
        /// 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("since")]
        public string Since { get; set; }
        /// <summary>
        /// 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("before")]
        public string Before { get; set; }
        /// <summary>
        /// 指定一组通知 ID，以 , 分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("ids")]
        public string Ids { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}