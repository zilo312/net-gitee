using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 检查指定用户是否关注目标用户
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UsersUsernameFollowingTargetUserRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 目标用户的用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("target_user")]
        public string TargetUser { get; set; }

    }
}