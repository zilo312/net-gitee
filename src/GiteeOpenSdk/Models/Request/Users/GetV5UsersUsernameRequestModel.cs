using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 获取一个用户
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UsersUsernameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }

    }
}