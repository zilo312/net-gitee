using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 添加一个公钥
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5UserKeysRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 公钥内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("key")]
        public string Key { get; set; }
        /// <summary>
        /// 公钥名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }

    }
}