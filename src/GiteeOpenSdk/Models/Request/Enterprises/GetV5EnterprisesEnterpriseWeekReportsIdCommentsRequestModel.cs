using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 某个周报评论列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseWeekReportsIdCommentsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 周报ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }

    }
}