using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 新建周报
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5EnterprisesEnterpriseWeekReportRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 周报内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("week_index")]
        public int WeekIndex { get; set; }
        /// <summary>
        /// 周报日期(格式：2019-03-25)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("date")]
        public string Date { get; set; }

    }
}