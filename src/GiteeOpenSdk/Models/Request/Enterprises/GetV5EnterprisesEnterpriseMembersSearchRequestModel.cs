using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 获取企业成员信息(通过用户名/邮箱)
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseMembersSearchRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 查询类型：username/email
        /// </summary>
        [RequestType("query")]
        [JsonProperty("query_type")]
        public string QueryType { get; set; }
        /// <summary>
        /// 查询值
        /// </summary>
        [RequestType("query")]
        [JsonProperty("query_value")]
        public string QueryValue { get; set; }

    }
}