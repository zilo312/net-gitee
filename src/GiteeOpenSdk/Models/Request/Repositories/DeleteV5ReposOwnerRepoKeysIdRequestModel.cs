using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 删除一个仓库公钥
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteV5ReposOwnerRepoKeysIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 公钥 ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }

    }
}