using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 创建一个仓库的 Tag
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoTagsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 起点名称, 默认：master
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("refs")]
        public string Refs { get; set; }
        /// <summary>
        /// 新创建的标签名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tag_name")]
        public string TagName { get; set; }
        /// <summary>
        /// Tag 描述, 默认为空
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tag_message")]
        public string TagMessage { get; set; }

    }
}