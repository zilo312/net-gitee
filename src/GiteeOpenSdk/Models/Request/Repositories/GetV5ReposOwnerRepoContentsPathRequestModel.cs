using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 获取仓库具体路径下的内容
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoContentsPathRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 文件的路径
        /// </summary>
        [RequestType("path")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 分支、tag或commit。默认: 仓库的默认分支(通常是master)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("ref")]
        public string Ref { get; set; }

    }
}