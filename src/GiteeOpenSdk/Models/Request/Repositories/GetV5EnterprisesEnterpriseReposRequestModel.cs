using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 获取企业的所有仓库
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseReposRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 筛选仓库的类型，可以是 all, public, internal, private。默认: all
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 只获取直属仓库，默认: false
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direct")]
        public bool? Direct { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}