using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 获取最近30天的七日以内访问量
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoTrafficDataRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 访问量的开始时间，默认今天，格式：yyyy-MM-dd
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("start_day")]
        public string StartDay { get; set; }
        /// <summary>
        /// 访问量的结束时间，默认七天前，格式：yyyy-MM-dd
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("end_day")]
        public string EndDay { get; set; }

    }
}