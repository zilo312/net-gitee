using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 仓库的所有提交
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoCommitsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 提交起始的SHA值或者分支名. 默认: 仓库的默认分支
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 包含该文件的提交
        /// </summary>
        [RequestType("query")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 提交作者的邮箱或个人空间地址(username/login)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author")]
        public string Author { get; set; }
        /// <summary>
        /// 提交的起始时间，时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("since")]
        public string Since { get; set; }
        /// <summary>
        /// 提交的最后时间，时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("until")]
        public string Until { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}