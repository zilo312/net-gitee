using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 上传设置 Pages SSL 证书和域名
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5ReposOwnerRepoPagesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 自定义域名
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("domain")]
        public string Domain { get; set; }
        /// <summary>
        /// 证书文件内容（需进行BASE64编码）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ssl_certificate_crt")]
        public string SslCertificateCrt { get; set; }
        /// <summary>
        /// 私钥文件内容（需进行BASE64编码）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ssl_certificate_key")]
        public string SslCertificateKey { get; set; }

    }
}