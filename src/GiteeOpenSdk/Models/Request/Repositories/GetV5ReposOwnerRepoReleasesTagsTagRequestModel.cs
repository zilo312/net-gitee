using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 根据Tag名称获取仓库的Release
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoReleasesTagsTagRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Tag 名称
        /// </summary>
        [RequestType("path")]
        [JsonProperty("tag")]
        public string Tag { get; set; }

    }
}