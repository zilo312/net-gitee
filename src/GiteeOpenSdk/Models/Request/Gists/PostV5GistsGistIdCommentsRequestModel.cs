using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Gists
{
    /// <summary>
    /// 增加代码片段的评论
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5GistsGistIdCommentsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 代码片段的ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("gist_id")]
        public string GistId { get; set; }
        /// <summary>
        /// 评论内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }

    }
}