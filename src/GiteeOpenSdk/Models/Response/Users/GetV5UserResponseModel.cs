﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 获取授权用户的资料返回模型
    /// </summary>
    public class GetV5UserResponseModel : GiteeResponseModel
    {
        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }
        [JsonProperty("bio")]
        public string Bio { get; set; }
        [JsonProperty("blog")]
        public string Blog { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("events_url")]
        public string EventsUrl { get; set; }
        [JsonProperty("followers")]
        public string Followers { get; set; }
        [JsonProperty("followers_url")]
        public string FollowersUrl { get; set; }
        [JsonProperty("following")]
        public string Following { get; set; }
        [JsonProperty("following_url")]
        public string FollowingUrl { get; set; }
        [JsonProperty("gists_url")]
        public string GistsUrl { get; set; }
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("login")]
        public string Login { get; set; }
        [JsonProperty("member_role")]
        public string MemberRole { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("organizations_url")]
        public string OrganizationsUrl { get; set; }
        [JsonProperty("public_gists")]
        public string PublicGists { get; set; }
        [JsonProperty("public_repos")]
        public string PublicRepos { get; set; }
        [JsonProperty("received_events_url")]
        public string ReceivedEventsUrl { get; set; }
        [JsonProperty("repos_url")]
        public string ReposUrl { get; set; }
        [JsonProperty("stared")]
        public string Stared { get; set; }
        [JsonProperty("starred_url")]
        public string StarredUrl { get; set; }
        [JsonProperty("subscriptions_url")]
        public string SubscriptionsUrl { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("watched")]
        public string Watched { get; set; }
        [JsonProperty("weibo")]
        public string Weibo { get; set; }

    }
}
