﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出指定用户的所有公钥
    /// </summary>
    public class GetV5UsersUsernameKeysResponseModel : GiteeResponseModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }

    }
}
