﻿using GiteeOpenSdk.Models.BaseModels;

namespace GiteeOpenSdk.Models.Response.Respositories
{
    /// <summary>
    /// 获取所有分支
    /// </summary>
    public class GetV5ReposOwnerRepoBranchesResponseModel : BaseBranchModel
    {
    }
}
