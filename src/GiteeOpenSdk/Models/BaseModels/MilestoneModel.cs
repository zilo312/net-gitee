﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// 里程碑模型
    /// </summary>
    public class MilestoneModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        [JsonProperty("number")]
        public int Number { get; set; }
        [JsonProperty("repository_id")]
        public int RepositoryId { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("open_issues")]
        public int OpenIssues { get; set; }
        [JsonProperty("closed_issues")]
        public int ClosedIssues { get; set; }
        [JsonProperty("due_on")]
        public DateTime DueOn { get; set; }
    }
}
