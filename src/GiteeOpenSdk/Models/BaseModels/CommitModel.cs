﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    public class CommitModel
    {
        [JsonProperty("sha")]
        public string Sha { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
