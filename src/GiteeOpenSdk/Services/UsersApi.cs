﻿using System;
using System.Threading.Tasks;
using GiteeOpenSdk.Models;
using GiteeOpenSdk.Models.Request.Users;
using GiteeOpenSdk.Models.Response.Users;

namespace GiteeOpenSdk.Services
{
    /// <summary>
    /// 用户账号Api
    /// </summary>
    public class UsersApi : GiteeCommonApi
    {
        public UsersApi() { }
        public UsersApi(string clientId, string clientSecret, string accessToken) : base(clientId, clientSecret, accessToken) { }
        /// <summary>
        /// 列出授权用户的所有公钥
        /// </summary>
        public async Task<ResponseModels<GetV5UserKeysIdResponseModel>> GetV5UserKeys(GetV5UserKeysRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UserKeysRequestModel, GetV5UserKeysIdResponseModel>("api/v5/user/keys", model);
            return result;
        }
        /// <summary>
        /// 添加一个公钥
        /// </summary>
        public async Task<ResponseModel<PostV5UserKeysResponseModel>> PostV5UserKeys(PostV5UserKeysRequestModel model)
        {
            var result = await ExecuteAsync<PostV5UserKeysRequestModel, PostV5UserKeysResponseModel>("user/keys", model);
            return result;
        }
        /// <summary>
        /// 获取一个公钥
        /// </summary>
        public async Task<ResponseModel<GetV5UserKeysIdResponseModel>> GetV5UserKeysId(GetV5UserKeysIdRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UserKeysIdRequestModel, GetV5UserKeysIdResponseModel>("user/keys/{id}", model);
            return result;
        }
        /// <summary>
        /// 删除一个公钥
        /// </summary>
        public async Task<ResponseModel<DeleteV5UserKeysIdResponseModel>> DeleteV5UserKeysId(DeleteV5UserKeysIdRequestModel model)
        {
            var result = await ExecuteAsync<DeleteV5UserKeysIdRequestModel, DeleteV5UserKeysIdResponseModel>("user/keys/{id}", model);
            return result;
        }
        /// <summary>
        /// 获取授权用户的资料
        /// </summary>
        public async Task<ResponseModel<GetV5UserResponseModel>> GetV5User(GetV5UserRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UserRequestModel, GetV5UserResponseModel>("user", model);
            return result;
        }
        /// <summary>
        /// 更新授权用户的资料
        /// </summary>
        public async Task<ResponseModel<PatchV5UserResponseModel>> PatchV5User(PatchV5UserRequestModel model)
        {
            var result = await ExecuteAsync<PatchV5UserRequestModel, PatchV5UserResponseModel>("user", model);
            return result;
        }
        /// <summary>
        /// 列出授权用户的关注者
        /// </summary>
        public async Task<ResponseModels<GetV5UserFollowersResponseModel>> GetV5UserFollowers(GetV5UserFollowersRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UserFollowersRequestModel, GetV5UserFollowersResponseModel>("user/followers", model);
            return result;
        }
        /// <summary>
        /// 列出授权用户正关注的用户
        /// </summary>
        public async Task<ResponseModels<GetV5UserFollowingResponseModel>> GetV5UserFollowing(GetV5UserFollowingRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UserFollowingRequestModel, GetV5UserFollowingResponseModel>("user/following", model);
            return result;
        }
        /// <summary>
        /// 列出授权用户所有的 Namespace
        /// </summary>
        public async Task<ResponseModels<GetV5UserNamespacesResponseModel>> GetV5UserNamespaces(GetV5UserNamespacesRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UserNamespacesRequestModel, GetV5UserNamespacesResponseModel>("user/namespaces", model);
            return result;
        }
        /// <summary>
        /// 获取授权用户的一个 Namespace
        /// </summary>
        public async Task<ResponseModel<GetV5UserNamespaceResponseModel>> GetV5UserNamespace(GetV5UserNamespaceRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UserNamespaceRequestModel, GetV5UserNamespaceResponseModel>("user/namespace", model);
            return result;
        }
        /// <summary>
        /// 检查授权用户是否关注了一个用户
        /// </summary>
        public async Task<ResponseModel<GetV5UserFollowingUsernameResponseModel>> GetV5UserFollowingUsername(GetV5UserFollowingUsernameRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UserFollowingUsernameRequestModel, GetV5UserFollowingUsernameResponseModel>("user/following/{username}", model);
            return result;
        }
        /// <summary>
        /// 关注一个用户
        /// </summary>
        public async Task<ResponseModel<PutV5UserFollowingUsernameResponseModel>> PutV5UserFollowingUsername(PutV5UserFollowingUsernameRequestModel model)
        {
            var result = await ExecuteAsync<PutV5UserFollowingUsernameRequestModel, PutV5UserFollowingUsernameResponseModel>("user/following/{username}", model);
            return result;
        }
        /// <summary>
        /// 取消关注一个用户
        /// </summary>
        public async Task<ResponseModel<DeleteV5UserFollowingUsernameResponseModel>> DeleteV5UserFollowingUsername(DeleteV5UserFollowingUsernameRequestModel model)
        {
            var result = await ExecuteAsync<DeleteV5UserFollowingUsernameRequestModel, DeleteV5UserFollowingUsernameResponseModel>("user/following/{username}", model);
            return result;
        }
        /// <summary>
        /// 获取一个用户
        /// </summary>
        public async Task<ResponseModel<GetV5UsersUsernameResponseModel>> GetV5UsersUsername(GetV5UsersUsernameRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UsersUsernameRequestModel, GetV5UsersUsernameResponseModel>("users/{username}", model);
            return result;
        }
        /// <summary>
        /// 列出指定用户的关注者
        /// </summary>
        public async Task<ResponseModels<GetV5UsersUsernameFollowersResponseModel>> GetV5UsersUsernameFollowers(GetV5UsersUsernameFollowersRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UsersUsernameFollowersRequestModel, GetV5UsersUsernameFollowersResponseModel>("users/{username}/followers", model);
            return result;
        }
        /// <summary>
        /// 列出指定用户正在关注的用户
        /// </summary>
        public async Task<ResponseModels<GetV5UsersUsernameFollowingResponseModel>> GetV5UsersUsernameFollowing(GetV5UsersUsernameFollowingRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UsersUsernameFollowingRequestModel, GetV5UsersUsernameFollowingResponseModel>("users/{username}/following", model);
            return result;
        }
        /// <summary>
        /// 检查指定用户是否关注目标用户
        /// </summary>
        public async Task<ResponseModel<GetV5UsersUsernameFollowingTargetUserResponseModel>> GetV5UsersUsernameFollowingTargetUser(GetV5UsersUsernameFollowingTargetUserRequestModel model)
        {
            var result = await ExecuteAsync<GetV5UsersUsernameFollowingTargetUserRequestModel, GetV5UsersUsernameFollowingTargetUserResponseModel>("users/{username}/following/{target_user}", model);
            return result;
        }
        /// <summary>
        /// 列出指定用户的所有公钥
        /// </summary>
        public async Task<ResponseModels<GetV5UsersUsernameKeysResponseModel>> GetV5UsersUsernameKeys(GetV5UsersUsernameKeysRequestModel model)
        {
            var result = await ExecutesAsync<GetV5UsersUsernameKeysRequestModel, GetV5UsersUsernameKeysResponseModel>("users/{username}/keys", model);
            return result;
        }
    }
}
