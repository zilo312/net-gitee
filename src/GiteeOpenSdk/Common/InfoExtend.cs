﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Common
{
    public static class InfoExtend
    {
        /// <summary>
        /// 比较属性的参数类型
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="requestType"></param>
        /// <returns></returns>
        public static bool IsRequestType(this object propertyInfo, string requestType)
        {
            Type type = propertyInfo.GetType();
            var typeInfo = type.GetTypeInfo();

            object attr = typeInfo.GetCustomAttribute(typeof(RequestTypeAttribute), false);
            if (attr != null && ((RequestTypeAttribute)attr).RequestType == requestType)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 获取参数类型信息
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string ToRequestType(this PropertyInfo propertyInfo)
        {
            object attr = propertyInfo.GetCustomAttribute(typeof(RequestTypeAttribute), false);
            if (attr != null)
                return ((RequestTypeAttribute)attr).RequestType;
            else
                return default;
        }
        /// <summary>
        /// 获取JsonProperty信息
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string ToJsonInfo(this PropertyInfo propertyInfo)
        {
            object attr = propertyInfo.GetCustomAttribute(typeof(JsonPropertyAttribute), false);
            if (attr != null)
                return ((JsonPropertyAttribute)attr).PropertyName;
            else
                return propertyInfo.Name;
        }

        /// <summary>
        /// 获取参数方法信息
        /// </summary>
        /// <param name="classInfo"></param>
        /// <returns></returns>
        public static string ToRequestMethod(this object classInfo)
        {
            Type type = classInfo.GetType();
            var typeInfo = type.GetTypeInfo();

            object attr = typeInfo.GetCustomAttribute(typeof(RequestMethodAttribute), false);
            if (attr != null)
                return ((RequestMethodAttribute)attr).RequestMethod;
            else
                return default;
        }

        /// <summary>
        /// Dictionary Parse To String
        /// </summary>
        /// <param name="parameters">Dictionary</param>
        /// <returns>String</returns>
        public static string ParseToString(this IDictionary<string, string> parameters)
        {
            IDictionary<string, string> sortedParams = new SortedDictionary<string, string>(parameters);
            IEnumerator<KeyValuePair<string, string>> dem = sortedParams.GetEnumerator();

            StringBuilder query = new StringBuilder("");
            while (dem.MoveNext())
            {
                string key = dem.Current.Key;
                string value = dem.Current.Value;
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                {
                    query.Append(key).Append("=").Append(value).Append("&");
                }
            }
            string content = query.ToString().Substring(0, query.Length - 1);

            return content;
        }
    }
}
