/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class WeekReportsApiController : ControllerBase
    { 
        /// <summary>
        /// 删除周报某个评论
        /// </summary>
        /// <remarks>删除周报某个评论</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="reportId">周报的ID</param>
        /// <param name="commentId">评论ID</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="204">返回格式</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{report_id}/comment/{comment_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdWeekReportsReportIdCommentCommentId")]
        public virtual IActionResult DeleteEnterpriseIdWeekReportsReportIdCommentCommentId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? reportId, [FromRoute][Required]int? commentId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 删除周报模版
        /// </summary>
        /// <remarks>删除周报模版</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="204">删除周报模版</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/templates/{template_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdWeekReportsTemplatesTemplateId")]
        public virtual IActionResult DeleteEnterpriseIdWeekReportsTemplatesTemplateId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? templateId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 成员周报列表
        /// </summary>
        /// <remarks>成员周报列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="year">年份</param>
        /// <param name="weekIndex">周报所属周</param>
        /// <param name="programId">项目ID</param>
        /// <param name="groupId">团队ID</param>
        /// <param name="memberIds">成员ID，逗号分隔，如：1,2</param>
        /// <param name="date">周报日期(格式：2019-03-25)</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/member_reports")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsMemberReports")]
        [SwaggerResponse(statusCode: 200, type: typeof(MemberWeekReport), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsMemberReports([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? year, [FromQuery]int? weekIndex, [FromQuery]int? programId, [FromQuery]int? groupId, [FromQuery]string memberIds, [FromQuery]string date, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MemberWeekReport));
            string exampleJson = null;
            exampleJson = "{\n  \"month\" : 1,\n  \"year\" : 6,\n  \"id\" : 0,\n  \"week_index\" : 5,\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MemberWeekReport>(exampleJson)
                        : default(MemberWeekReport);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取授权用户的周报列表
        /// </summary>
        /// <remarks>获取授权用户的周报列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="year">年份</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/my_reports")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsMyReports")]
        [SwaggerResponse(statusCode: 200, type: typeof(MyWeekReport), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsMyReports([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? year)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MyWeekReport));
            string exampleJson = null;
            exampleJson = "{\n  \"month\" : 1,\n  \"year\" : 6,\n  \"id\" : 0,\n  \"week_index\" : 5,\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MyWeekReport>(exampleJson)
                        : default(MyWeekReport);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取未提交周报的用户列表
        /// </summary>
        /// <remarks>获取未提交周报的用户列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="year">年份</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="weekIndex">当前周数（默认当前周）</param>
        /// <param name="programId">项目的 id</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/not_submit_users")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsNotSubmitUsers")]
        [SwaggerResponse(statusCode: 200, type: typeof(MyWeekReport), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsNotSubmitUsers([FromRoute][Required]int? enterpriseId, [FromQuery][Required()]int? year, [FromQuery]string accessToken, [FromQuery]int? weekIndex, [FromQuery]int? programId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MyWeekReport));
            string exampleJson = null;
            exampleJson = "{\n  \"month\" : 1,\n  \"year\" : 6,\n  \"id\" : 0,\n  \"week_index\" : 5,\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MyWeekReport>(exampleJson)
                        : default(MyWeekReport);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 周报可关联动态列表
        /// </summary>
        /// <remarks>周报可关联动态列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="year">周报所属年</param>
        /// <param name="weekIndex">周报所属周</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="eventIds">一关联的动态ids， 用逗号分隔，如：1,2,3</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/relate_events")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsRelateEvents")]
        [SwaggerResponse(statusCode: 200, type: typeof(Event), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsRelateEvents([FromRoute][Required]int? enterpriseId, [FromQuery][Required()]int? year, [FromQuery][Required()]int? weekIndex, [FromQuery]string accessToken, [FromQuery]int? eventIds, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(Event));
            string exampleJson = null;
            exampleJson = "{\n  \"in_enterprise\" : true,\n  \"ident\" : \"ident\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"enterprise\" : \"enterprise\",\n  \"is_proper\" : true,\n  \"target_type\" : \"target_type\",\n  \"created_at\" : \"created_at\",\n  \"project\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"action_human_name\" : \"action_human_name\",\n  \"payload\" : { },\n  \"in_project\" : true,\n  \"action\" : \"action\",\n  \"id\" : 0\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Event>(exampleJson)
                        : default(Event);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取周报可关联issue和pull request
        /// </summary>
        /// <remarks>获取周报可关联issue和pull request</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="year">周报所属年</param>
        /// <param name="weekIndex">周报所属周</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/relations")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsRelations")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportRelation), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsRelations([FromRoute][Required]int? enterpriseId, [FromQuery][Required()]int? year, [FromQuery][Required()]int? weekIndex, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportRelation));
            string exampleJson = null;
            exampleJson = "{\n  \"pull_requests\" : {\n    \"pr_assign_num\" : 9,\n    \"close_related_issue\" : 3,\n    \"pr_test_num\" : 4,\n    \"iid\" : 6,\n    \"check_state\" : 5,\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"target_branch\" : { },\n    \"assignees\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 2,\n      \"state\" : 7,\n      \"username\" : \"username\"\n    },\n    \"prune_branch\" : 2,\n    \"can_merge\" : \"can_merge\",\n    \"title\" : \"title\",\n    \"test_state\" : 5,\n    \"priority\" : \"priority\",\n    \"latest_scan_task\" : { },\n    \"lightweight\" : true,\n    \"labels\" : {\n      \"color\" : \"color\",\n      \"name\" : \"name\",\n      \"id\" : 2\n    },\n    \"source_branch\" : { },\n    \"project_id\" : 1,\n    \"id\" : 0,\n    \"state\" : \"state\",\n    \"conflict\" : true\n  },\n  \"issues\" : {\n    \"duration\" : 3,\n    \"ident\" : \"ident\",\n    \"comments_count\" : 7,\n    \"priority_human\" : \"priority_human\",\n    \"root_id\" : 2,\n    \"is_overdue\" : true,\n    \"id\" : 5,\n    \"state\" : \"state\",\n    \"title\" : \"title\",\n    \"priority\" : 9\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportRelation>(exampleJson)
                        : default(WeekReportRelation);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 某个周报评论列表
        /// </summary>
        /// <remarks>某个周报评论列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="reportId">周报的ID</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{report_id}/comments")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsReportIdComments")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportNote), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsReportIdComments([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? reportId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportNote));
            string exampleJson = null;
            exampleJson = "{\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"reactions\" : {\n    \"user_id\" : 5,\n    \"target_type\" : \"target_type\",\n    \"target_id\" : 1,\n    \"id\" : 6,\n    \"text\" : \"text\"\n  },\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportNote>(exampleJson)
                        : default(WeekReportNote);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 周报模版列表
        /// </summary>
        /// <remarks>周报模版列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/templates")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsTemplates")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportTemplate), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsTemplates([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportTemplate));
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_default\" : true,\n  \"content\" : \"content\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportTemplate>(exampleJson)
                        : default(WeekReportTemplate);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 查看周报模版详情
        /// </summary>
        /// <remarks>查看周报模版详情</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/templates/{template_id}")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsTemplatesTemplateId")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportTemplate), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsTemplatesTemplateId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? templateId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportTemplate));
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_default\" : true,\n  \"content\" : \"content\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportTemplate>(exampleJson)
                        : default(WeekReportTemplate);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取某人某年某周的周报详情
        /// </summary>
        /// <remarks>获取某人某年某周的周报详情</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="username">用户个性域名</param>
        /// <param name="year">周报所处年</param>
        /// <param name="weekIndex">周报所处周</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{username}/{year}/{week_index}")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsUsernameYearWeekIndex")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportDetail), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdWeekReportsUsernameYearWeekIndex([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string username, [FromRoute][Required]int? year, [FromRoute][Required]int? weekIndex, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportDetail));
            string exampleJson = null;
            exampleJson = "{\n  \"pull_requests\" : {\n    \"pr_assign_num\" : 9,\n    \"close_related_issue\" : 3,\n    \"pr_test_num\" : 4,\n    \"iid\" : 6,\n    \"check_state\" : 5,\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"target_branch\" : { },\n    \"assignees\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 2,\n      \"state\" : 7,\n      \"username\" : \"username\"\n    },\n    \"prune_branch\" : 2,\n    \"can_merge\" : \"can_merge\",\n    \"title\" : \"title\",\n    \"test_state\" : 5,\n    \"priority\" : \"priority\",\n    \"latest_scan_task\" : { },\n    \"lightweight\" : true,\n    \"labels\" : {\n      \"color\" : \"color\",\n      \"name\" : \"name\",\n      \"id\" : 2\n    },\n    \"source_branch\" : { },\n    \"project_id\" : 1,\n    \"id\" : 0,\n    \"state\" : \"state\",\n    \"conflict\" : true\n  },\n  \"month\" : 1,\n  \"year\" : 6,\n  \"id\" : 0,\n  \"week_index\" : 5,\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issues\" : {\n    \"duration\" : 3,\n    \"ident\" : \"ident\",\n    \"comments_count\" : 7,\n    \"priority_human\" : \"priority_human\",\n    \"root_id\" : 2,\n    \"is_overdue\" : true,\n    \"id\" : 5,\n    \"state\" : \"state\",\n    \"title\" : \"title\",\n    \"priority\" : 9\n  },\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\",\n  \"events\" : {\n    \"in_enterprise\" : true,\n    \"ident\" : \"ident\",\n    \"author\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 6,\n      \"username\" : \"username\"\n    },\n    \"enterprise\" : \"enterprise\",\n    \"is_proper\" : true,\n    \"target_type\" : \"target_type\",\n    \"created_at\" : \"created_at\",\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"action_human_name\" : \"action_human_name\",\n    \"payload\" : { },\n    \"in_project\" : true,\n    \"action\" : \"action\",\n    \"id\" : 0\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportDetail>(exampleJson)
                        : default(WeekReportDetail);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取企业某年存在周报的周
        /// </summary>
        /// <remarks>获取企业某年存在周报的周</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="year">周报所属年</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取企业某年存在周报的周</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{year}/weeks")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsYearWeeks")]
        public virtual IActionResult GetEnterpriseIdWeekReportsYearWeeks([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? year, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取企业存在周报的年份
        /// </summary>
        /// <remarks>获取企业存在周报的年份</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取企业存在周报的年份</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/years")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdWeekReportsYears")]
        public virtual IActionResult GetEnterpriseIdWeekReportsYears([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 预览周报
        /// </summary>
        /// <remarks>预览周报</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/preview_data")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdWeekReportsPreviewData")]
        [SwaggerResponse(statusCode: 201, type: typeof(WeekReportPreview), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdWeekReportsPreviewData([FromBody]Body72 body, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(WeekReportPreview));
            string exampleJson = null;
            exampleJson = "{\n  \"pull_requests\" : {\n    \"pr_assign_num\" : 9,\n    \"close_related_issue\" : 3,\n    \"pr_test_num\" : 4,\n    \"iid\" : 6,\n    \"check_state\" : 5,\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"target_branch\" : { },\n    \"assignees\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 2,\n      \"state\" : 7,\n      \"username\" : \"username\"\n    },\n    \"prune_branch\" : 2,\n    \"can_merge\" : \"can_merge\",\n    \"title\" : \"title\",\n    \"test_state\" : 5,\n    \"priority\" : \"priority\",\n    \"latest_scan_task\" : { },\n    \"lightweight\" : true,\n    \"labels\" : {\n      \"color\" : \"color\",\n      \"name\" : \"name\",\n      \"id\" : 2\n    },\n    \"source_branch\" : { },\n    \"project_id\" : 1,\n    \"id\" : 0,\n    \"state\" : \"state\",\n    \"conflict\" : true\n  },\n  \"issues\" : {\n    \"duration\" : 3,\n    \"ident\" : \"ident\",\n    \"comments_count\" : 7,\n    \"priority_human\" : \"priority_human\",\n    \"root_id\" : 2,\n    \"is_overdue\" : true,\n    \"id\" : 5,\n    \"state\" : \"state\",\n    \"title\" : \"title\",\n    \"priority\" : 9\n  },\n  \"events\" : {\n    \"in_enterprise\" : true,\n    \"ident\" : \"ident\",\n    \"author\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 6,\n      \"username\" : \"username\"\n    },\n    \"enterprise\" : \"enterprise\",\n    \"is_proper\" : true,\n    \"target_type\" : \"target_type\",\n    \"created_at\" : \"created_at\",\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"action_human_name\" : \"action_human_name\",\n    \"payload\" : { },\n    \"in_project\" : true,\n    \"action\" : \"action\",\n    \"id\" : 0\n  },\n  \"content\" : \"content\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportPreview>(exampleJson)
                        : default(WeekReportPreview);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 评论周报
        /// </summary>
        /// <remarks>评论周报</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="reportId">周报的ID</param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{report_id}/comment")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdWeekReportsReportIdComment")]
        [SwaggerResponse(statusCode: 201, type: typeof(WeekReportNote), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdWeekReportsReportIdComment([FromBody]Body75 body, [FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? reportId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(WeekReportNote));
            string exampleJson = null;
            exampleJson = "{\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"reactions\" : {\n    \"user_id\" : 5,\n    \"target_type\" : \"target_type\",\n    \"target_id\" : 1,\n    \"id\" : 6,\n    \"text\" : \"text\"\n  },\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportNote>(exampleJson)
                        : default(WeekReportNote);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 新增周报模版
        /// </summary>
        /// <remarks>新增周报模版</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/templates")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdWeekReportsTemplates")]
        [SwaggerResponse(statusCode: 201, type: typeof(WeekReportTemplate), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdWeekReportsTemplates([FromBody]Body73 body, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(WeekReportTemplate));
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_default\" : true,\n  \"content\" : \"content\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportTemplate>(exampleJson)
                        : default(WeekReportTemplate);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 编辑周报模版
        /// </summary>
        /// <remarks>编辑周报模版</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="body"></param>
        /// <response code="200">返回格式</response>
        [HttpPut]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/templates/{template_id}")]
        [ValidateModelState]
        [SwaggerOperation("PutEnterpriseIdWeekReportsTemplatesTemplateId")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportTemplate), description: "返回格式")]
        public virtual IActionResult PutEnterpriseIdWeekReportsTemplatesTemplateId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? templateId, [FromBody]Body74 body)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportTemplate));
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_default\" : true,\n  \"content\" : \"content\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportTemplate>(exampleJson)
                        : default(WeekReportTemplate);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 新建/编辑周报
        /// </summary>
        /// <remarks>新建/编辑周报</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="year">周报所属年</param>
        /// <param name="weekIndex">周报所属周</param>
        /// <param name="username">用户名(username/login)</param>
        /// <response code="200">返回格式</response>
        [HttpPut]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/week_reports/{username}/{year}/{week_index}")]
        [ValidateModelState]
        [SwaggerOperation("PutEnterpriseIdWeekReportsUsernameYearWeekIndex")]
        [SwaggerResponse(statusCode: 200, type: typeof(WeekReportDetail), description: "返回格式")]
        public virtual IActionResult PutEnterpriseIdWeekReportsUsernameYearWeekIndex([FromBody]Body71 body, [FromRoute][Required]int? enterpriseId, [FromRoute][Required]int? year, [FromRoute][Required]int? weekIndex, [FromRoute][Required]string username)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(WeekReportDetail));
            string exampleJson = null;
            exampleJson = "{\n  \"pull_requests\" : {\n    \"pr_assign_num\" : 9,\n    \"close_related_issue\" : 3,\n    \"pr_test_num\" : 4,\n    \"iid\" : 6,\n    \"check_state\" : 5,\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"target_branch\" : { },\n    \"assignees\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 2,\n      \"state\" : 7,\n      \"username\" : \"username\"\n    },\n    \"prune_branch\" : 2,\n    \"can_merge\" : \"can_merge\",\n    \"title\" : \"title\",\n    \"test_state\" : 5,\n    \"priority\" : \"priority\",\n    \"latest_scan_task\" : { },\n    \"lightweight\" : true,\n    \"labels\" : {\n      \"color\" : \"color\",\n      \"name\" : \"name\",\n      \"id\" : 2\n    },\n    \"source_branch\" : { },\n    \"project_id\" : 1,\n    \"id\" : 0,\n    \"state\" : \"state\",\n    \"conflict\" : true\n  },\n  \"month\" : 1,\n  \"year\" : 6,\n  \"id\" : 0,\n  \"week_index\" : 5,\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issues\" : {\n    \"duration\" : 3,\n    \"ident\" : \"ident\",\n    \"comments_count\" : 7,\n    \"priority_human\" : \"priority_human\",\n    \"root_id\" : 2,\n    \"is_overdue\" : true,\n    \"id\" : 5,\n    \"state\" : \"state\",\n    \"title\" : \"title\",\n    \"priority\" : 9\n  },\n  \"content\" : \"content\",\n  \"content_html\" : \"content_html\",\n  \"events\" : {\n    \"in_enterprise\" : true,\n    \"ident\" : \"ident\",\n    \"author\" : {\n      \"pinyin\" : \"pinyin\",\n      \"avatar_url\" : \"avatar_url\",\n      \"name\" : \"name\",\n      \"is_enterprise_member\" : true,\n      \"outsourced\" : true,\n      \"remark\" : \"remark\",\n      \"id\" : 6,\n      \"username\" : \"username\"\n    },\n    \"enterprise\" : \"enterprise\",\n    \"is_proper\" : true,\n    \"target_type\" : \"target_type\",\n    \"created_at\" : \"created_at\",\n    \"project\" : {\n      \"path\" : \"path\",\n      \"public\" : 5,\n      \"name\" : \"name\",\n      \"id\" : 1,\n      \"enterprise_id\" : 5\n    },\n    \"action_human_name\" : \"action_human_name\",\n    \"payload\" : { },\n    \"in_project\" : true,\n    \"action\" : \"action\",\n    \"id\" : 0\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<WeekReportDetail>(exampleJson)
                        : default(WeekReportDetail);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
