/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class ProgramsApiController : ControllerBase
    { 
        /// <summary>
        /// 删除项目
        /// </summary>
        /// <remarks>删除项目</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="deleteMilestones">是否删除里程碑</param>
        /// <param name="deleteIssues">是否删除任务</param>
        /// <response code="204">删除项目</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProgramsProgramId")]
        public virtual IActionResult DeleteEnterpriseIdProgramsProgramId([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]bool? deleteMilestones, [FromQuery]bool? deleteIssues)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 移出项目下成员
        /// </summary>
        /// <remarks>移出项目下成员</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="memberUserId">成员id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="204">移出项目下成员</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/members/{member_user_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProgramsProgramIdMembersMemberUserId")]
        public virtual IActionResult DeleteEnterpriseIdProgramsProgramIdMembersMemberUserId([FromRoute][Required]int? programId, [FromRoute][Required]int? memberUserId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 移出项目下仓库
        /// </summary>
        /// <remarks>移出项目下仓库</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="projectId">仓库id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="204">移出项目下仓库</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/projects/{project_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProgramsProgramIdProjectsProjectId")]
        public virtual IActionResult DeleteEnterpriseIdProgramsProgramIdProjectsProjectId([FromRoute][Required]int? programId, [FromRoute][Required]int? projectId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取项目列表
        /// </summary>
        /// <remarks>获取项目列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="type">筛选不同类型的项目列表。我参与的: joined; 我负责的: assigned; 我创建的: created</param>
        /// <param name="sort">排序字段(created_at: 创建时间 updated_at: 更新时间 users_count: 成员数 projects_count: 仓库数 issues_count: 任务数)</param>
        /// <param name="direction">排序方向(asc: 升序 desc: 倒序)</param>
        /// <param name="status">项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔,如: 0,1</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdPrograms")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<ProgramList>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdPrograms([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string type, [FromQuery]string sort, [FromQuery]string direction, [FromQuery]string status, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<ProgramList>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"users_count\" : 1,\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"projects_count\" : 5,\n  \"closed_issues_count\" : 2,\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"issues_count\" : 5,\n  \"status\" : 6\n}, {\n  \"users_count\" : 1,\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"projects_count\" : 5,\n  \"closed_issues_count\" : 2,\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"issues_count\" : 5,\n  \"status\" : 6\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<ProgramList>>(exampleJson)
                        : default(List<ProgramList>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目详情
        /// </summary>
        /// <remarks>获取项目详情</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramId")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Program>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramId([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<Program>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"status\" : 6\n}, {\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"status\" : 6\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<Program>>(exampleJson)
                        : default(List<Program>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目下动态列表
        /// </summary>
        /// <remarks>获取项目下动态列表</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="startDate">查询的起始时间。(格式：yyyy-mm-dd)</param>
        /// <param name="endDate">查询的结束时间。(格式：yyyy-mm-dd)</param>
        /// <param name="scope">项目范围：所有，仓库，任务，外部，我的</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <param name="prevId">上一次动态列表中最小动态 ID (返回列表不包含该ID记录)</param>
        /// <param name="limit">每次获取动态的条数</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/events")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdEvents")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Event>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdEvents([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string startDate, [FromQuery]string endDate, [FromQuery]string scope, [FromQuery]int? page, [FromQuery]int? perPage, [FromQuery]int? prevId, [FromQuery]int? limit)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<Event>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"in_enterprise\" : true,\n  \"ident\" : \"ident\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"enterprise\" : \"enterprise\",\n  \"is_proper\" : true,\n  \"target_type\" : \"target_type\",\n  \"created_at\" : \"created_at\",\n  \"project\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"action_human_name\" : \"action_human_name\",\n  \"payload\" : { },\n  \"in_project\" : true,\n  \"action\" : \"action\",\n  \"id\" : 0\n}, {\n  \"in_enterprise\" : true,\n  \"ident\" : \"ident\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"enterprise\" : \"enterprise\",\n  \"is_proper\" : true,\n  \"target_type\" : \"target_type\",\n  \"created_at\" : \"created_at\",\n  \"project\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"action_human_name\" : \"action_human_name\",\n  \"payload\" : { },\n  \"in_project\" : true,\n  \"action\" : \"action\",\n  \"id\" : 0\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<Event>>(exampleJson)
                        : default(List<Event>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目下的任务列表
        /// </summary>
        /// <remarks>获取项目下的任务列表</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId">企业 id</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="projectId">仓库 id</param>
        /// <param name="milestoneId">里程碑 id</param>
        /// <param name="state">任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing）</param>
        /// <param name="onlyRelatedMe">是否仅列出与授权用户相关的任务（0: 否 1: 是）</param>
        /// <param name="assigneeId">负责人 id</param>
        /// <param name="authorId">创建者 id</param>
        /// <param name="collaboratorIds">协作者。(,分隔的id字符串)</param>
        /// <param name="createdAt">创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）&lt;yyyymmddTHH:MM:SS+08:00，（大于指定日期）&gt;yyyymmddTHH:MM:SS+08:00</param>
        /// <param name="planStartedAt">计划开始时间，格式同上</param>
        /// <param name="deadline">任务截止日期，格式同上</param>
        /// <param name="finishedAt">任务完成日期，格式同上</param>
        /// <param name="search">搜索参数</param>
        /// <param name="filterChild">是否过滤子任务(0: 否, 1: 是)</param>
        /// <param name="issueStateIds">任务状态id，多选，用逗号分隔。</param>
        /// <param name="issueTypeId">任务类型</param>
        /// <param name="labelIds">标签 id（可多选，用逗号分隔）</param>
        /// <param name="priority">优先级（可多选，用逗号分隔）</param>
        /// <param name="sort">排序字段(created_at、updated_at、deadline、priority)</param>
        /// <param name="direction">排序方向(asc: 升序 desc: 倒序)</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/issues")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdIssues")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Issue>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdIssues([FromRoute][Required]string programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string projectId, [FromQuery]string milestoneId, [FromQuery]string state, [FromQuery]string onlyRelatedMe, [FromQuery]string assigneeId, [FromQuery]string authorId, [FromQuery]string collaboratorIds, [FromQuery]string createdAt, [FromQuery]string planStartedAt, [FromQuery]string deadline, [FromQuery]string finishedAt, [FromQuery]string search, [FromQuery]string filterChild, [FromQuery]string issueStateIds, [FromQuery]string issueTypeId, [FromQuery]string labelIds, [FromQuery]string priority, [FromQuery]string sort, [FromQuery]string direction, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<Issue>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"issue_state\" : {\n    \"color\" : \"color\",\n    \"serial\" : \"serial\",\n    \"icon\" : \"icon\",\n    \"id\" : 9,\n    \"title\" : \"title\",\n    \"command\" : \"command\"\n  },\n  \"ident\" : \"ident\",\n  \"priority_human\" : \"priority_human\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issue_type\" : {\n    \"template\" : \"template\",\n    \"is_system\" : true,\n    \"color\" : \"color\",\n    \"ident\" : \"ident\",\n    \"id\" : 3,\n    \"title\" : \"title\"\n  },\n  \"title\" : \"title\",\n  \"priority\" : 5,\n  \"branch\" : \"branch\",\n  \"labels\" : {\n    \"color\" : \"color\",\n    \"name\" : \"name\",\n    \"id\" : 2\n  },\n  \"duration\" : 2,\n  \"milestone\" : {\n    \"id\" : 7,\n    \"state\" : \"state\",\n    \"title\" : \"title\"\n  },\n  \"parent_id\" : 1,\n  \"comments_count\" : 5,\n  \"collaborators\" : \"\",\n  \"root_id\" : 6,\n  \"id\" : 0,\n  \"state\" : \"state\",\n  \"security_hole\" : true\n}, {\n  \"issue_state\" : {\n    \"color\" : \"color\",\n    \"serial\" : \"serial\",\n    \"icon\" : \"icon\",\n    \"id\" : 9,\n    \"title\" : \"title\",\n    \"command\" : \"command\"\n  },\n  \"ident\" : \"ident\",\n  \"priority_human\" : \"priority_human\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issue_type\" : {\n    \"template\" : \"template\",\n    \"is_system\" : true,\n    \"color\" : \"color\",\n    \"ident\" : \"ident\",\n    \"id\" : 3,\n    \"title\" : \"title\"\n  },\n  \"title\" : \"title\",\n  \"priority\" : 5,\n  \"branch\" : \"branch\",\n  \"labels\" : {\n    \"color\" : \"color\",\n    \"name\" : \"name\",\n    \"id\" : 2\n  },\n  \"duration\" : 2,\n  \"milestone\" : {\n    \"id\" : 7,\n    \"state\" : \"state\",\n    \"title\" : \"title\"\n  },\n  \"parent_id\" : 1,\n  \"comments_count\" : 5,\n  \"collaborators\" : \"\",\n  \"root_id\" : 6,\n  \"id\" : 0,\n  \"state\" : \"state\",\n  \"security_hole\" : true\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<Issue>>(exampleJson)
                        : default(List<Issue>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目下的成员列表
        /// </summary>
        /// <remarks>获取项目下的成员列表</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="search">搜索关键字</param>
        /// <param name="sort">排序字段(created_at: 创建时间 remark: 在企业的备注)</param>
        /// <param name="direction">排序方向(asc: 升序 desc: 倒序)</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/members")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdMembers")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Member>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdMembers([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string search, [FromQuery]string sort, [FromQuery]string direction, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<Member>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"enterprise_role\" : {\n    \"ident\" : \"ident\",\n    \"name\" : \"name\",\n    \"description\" : \"description\",\n    \"id\" : 5,\n    \"is_system_default\" : true,\n    \"is_default\" : true\n  },\n  \"occupation\" : \"occupation\",\n  \"is_block\" : true,\n  \"remark\" : \"remark\",\n  \"block_message\" : \"block_message\",\n  \"enterprise_version\" : 5,\n  \"is_guided\" : true,\n  \"pinyin\" : \"pinyin\",\n  \"phone\" : \"phone\",\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_feedback\" : true,\n  \"user\" : {\n    \"user_color\" : 1,\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"id\" : 6,\n    \"state\" : \"state\",\n    \"username\" : \"username\"\n  },\n  \"email\" : \"email\",\n  \"username\" : \"username\"\n}, {\n  \"enterprise_role\" : {\n    \"ident\" : \"ident\",\n    \"name\" : \"name\",\n    \"description\" : \"description\",\n    \"id\" : 5,\n    \"is_system_default\" : true,\n    \"is_default\" : true\n  },\n  \"occupation\" : \"occupation\",\n  \"is_block\" : true,\n  \"remark\" : \"remark\",\n  \"block_message\" : \"block_message\",\n  \"enterprise_version\" : 5,\n  \"is_guided\" : true,\n  \"pinyin\" : \"pinyin\",\n  \"phone\" : \"phone\",\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_feedback\" : true,\n  \"user\" : {\n    \"user_color\" : 1,\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"id\" : 6,\n    \"state\" : \"state\",\n    \"username\" : \"username\"\n  },\n  \"email\" : \"email\",\n  \"username\" : \"username\"\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<Member>>(exampleJson)
                        : default(List<Member>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目的操作权限
        /// </summary>
        /// <remarks>获取项目的操作权限</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取项目的操作权限</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/operate_auths")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdOperateAuths")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdOperateAuths([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取项目下的仓库列表
        /// </summary>
        /// <remarks>获取项目下的仓库列表</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="scope">范围筛选</param>
        /// <param name="search">搜索参数</param>
        /// <param name="type">与我相关，created：我创建的，joined：我参与的</param>
        /// <param name="status">状态: 0: 开始，1: 暂停，2: 关闭 </param>
        /// <param name="creatorId">负责人</param>
        /// <param name="parentId">form_from仓库id</param>
        /// <param name="forkFilter">非fork的(not_fork), 只看fork的(only_fork)</param>
        /// <param name="outsourced">是否外包：0：内部，1：外包</param>
        /// <param name="groupId">团队id</param>
        /// <param name="sort">排序字段(created_at、last_push_at)</param>
        /// <param name="direction">排序方向(asc: 升序 desc: 倒序)</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/projects")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdProjects")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<ProjectDetail>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdProjects([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string scope, [FromQuery]string search, [FromQuery]string type, [FromQuery]int? status, [FromQuery]int? creatorId, [FromQuery]int? parentId, [FromQuery]string forkFilter, [FromQuery]int? outsourced, [FromQuery]int? groupId, [FromQuery]string sort, [FromQuery]string direction, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<ProjectDetail>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"last_push_at\" : true,\n  \"description\" : \"description\",\n  \"repo_size\" : 2,\n  \"enable_backup\" : true,\n  \"path\" : \"path\",\n  \"public\" : 6,\n  \"has_backups\" : true,\n  \"stars_count\" : 3,\n  \"id\" : 0,\n  \"vip\" : true,\n  \"watches_count\" : 9,\n  \"is_fork\" : true,\n  \"creator\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"status_name\" : 5,\n  \"can_admin_project\" : true,\n  \"enterprise_id\" : 1,\n  \"parent_project\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"name\" : \"name\",\n  \"namespace\" : {\n    \"path\" : \"path\",\n    \"name\" : \"name\",\n    \"id\" : 5,\n    \"type\" : \"type\"\n  },\n  \"outsourced\" : true,\n  \"forked_count\" : 2,\n  \"recomm\" : true,\n  \"members_count\" : 7,\n  \"status\" : 5,\n  \"get_default_branch\" : \"get_default_branch\"\n}, {\n  \"last_push_at\" : true,\n  \"description\" : \"description\",\n  \"repo_size\" : 2,\n  \"enable_backup\" : true,\n  \"path\" : \"path\",\n  \"public\" : 6,\n  \"has_backups\" : true,\n  \"stars_count\" : 3,\n  \"id\" : 0,\n  \"vip\" : true,\n  \"watches_count\" : 9,\n  \"is_fork\" : true,\n  \"creator\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"status_name\" : 5,\n  \"can_admin_project\" : true,\n  \"enterprise_id\" : 1,\n  \"parent_project\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"name\" : \"name\",\n  \"namespace\" : {\n    \"path\" : \"path\",\n    \"name\" : \"name\",\n    \"id\" : 5,\n    \"type\" : \"type\"\n  },\n  \"outsourced\" : true,\n  \"forked_count\" : 2,\n  \"recomm\" : true,\n  \"members_count\" : 7,\n  \"status\" : 5,\n  \"get_default_branch\" : \"get_default_branch\"\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<ProjectDetail>>(exampleJson)
                        : default(List<ProjectDetail>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取项目下的 Pull Request
        /// </summary>
        /// <remarks>获取项目下的 Pull Request</remarks>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId">企业 id</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="state">PR 状态</param>
        /// <param name="scope">范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in</param>
        /// <param name="authorId">筛选 PR 创建者</param>
        /// <param name="assigneeId">筛选 PR 审查者</param>
        /// <param name="testerId">筛选 PR 测试人员</param>
        /// <param name="search">搜索参数</param>
        /// <param name="sort">排序字段(created_at、closed_at、priority)</param>
        /// <param name="direction">排序方向(asc: 升序 desc: 倒序)</param>
        /// <param name="groupId">团队 id</param>
        /// <param name="milestoneId">里程碑 id</param>
        /// <param name="labels">标签。多个标签逗号(,)隔开</param>
        /// <param name="canBeMerged">是否可合并</param>
        /// <param name="projectId">仓库 id</param>
        /// <param name="needStateCount">是否需要状态统计数</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/pull_requests")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsProgramIdPullRequests")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<Issue>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProgramsProgramIdPullRequests([FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]string state, [FromQuery]string scope, [FromQuery]string authorId, [FromQuery]string assigneeId, [FromQuery]string testerId, [FromQuery]string search, [FromQuery]string sort, [FromQuery]string direction, [FromQuery]int? groupId, [FromQuery]int? milestoneId, [FromQuery]string labels, [FromQuery]int? canBeMerged, [FromQuery]int? projectId, [FromQuery]int? needStateCount, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<Issue>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"issue_state\" : {\n    \"color\" : \"color\",\n    \"serial\" : \"serial\",\n    \"icon\" : \"icon\",\n    \"id\" : 9,\n    \"title\" : \"title\",\n    \"command\" : \"command\"\n  },\n  \"ident\" : \"ident\",\n  \"priority_human\" : \"priority_human\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issue_type\" : {\n    \"template\" : \"template\",\n    \"is_system\" : true,\n    \"color\" : \"color\",\n    \"ident\" : \"ident\",\n    \"id\" : 3,\n    \"title\" : \"title\"\n  },\n  \"title\" : \"title\",\n  \"priority\" : 5,\n  \"branch\" : \"branch\",\n  \"labels\" : {\n    \"color\" : \"color\",\n    \"name\" : \"name\",\n    \"id\" : 2\n  },\n  \"duration\" : 2,\n  \"milestone\" : {\n    \"id\" : 7,\n    \"state\" : \"state\",\n    \"title\" : \"title\"\n  },\n  \"parent_id\" : 1,\n  \"comments_count\" : 5,\n  \"collaborators\" : \"\",\n  \"root_id\" : 6,\n  \"id\" : 0,\n  \"state\" : \"state\",\n  \"security_hole\" : true\n}, {\n  \"issue_state\" : {\n    \"color\" : \"color\",\n    \"serial\" : \"serial\",\n    \"icon\" : \"icon\",\n    \"id\" : 9,\n    \"title\" : \"title\",\n    \"command\" : \"command\"\n  },\n  \"ident\" : \"ident\",\n  \"priority_human\" : \"priority_human\",\n  \"author\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"issue_type\" : {\n    \"template\" : \"template\",\n    \"is_system\" : true,\n    \"color\" : \"color\",\n    \"ident\" : \"ident\",\n    \"id\" : 3,\n    \"title\" : \"title\"\n  },\n  \"title\" : \"title\",\n  \"priority\" : 5,\n  \"branch\" : \"branch\",\n  \"labels\" : {\n    \"color\" : \"color\",\n    \"name\" : \"name\",\n    \"id\" : 2\n  },\n  \"duration\" : 2,\n  \"milestone\" : {\n    \"id\" : 7,\n    \"state\" : \"state\",\n    \"title\" : \"title\"\n  },\n  \"parent_id\" : 1,\n  \"comments_count\" : 5,\n  \"collaborators\" : \"\",\n  \"root_id\" : 6,\n  \"id\" : 0,\n  \"state\" : \"state\",\n  \"security_hole\" : true\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<Issue>>(exampleJson)
                        : default(List<Issue>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取未立项项目
        /// </summary>
        /// <remarks>获取未立项项目</remarks>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取未立项项目</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/unset")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProgramsUnset")]
        public virtual IActionResult GetEnterpriseIdProgramsUnset([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 新建项目
        /// </summary>
        /// <remarks>新建项目</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId"></param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdPrograms")]
        [SwaggerResponse(statusCode: 201, type: typeof(Program), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdPrograms([FromBody]Body9 body, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(Program));
            string exampleJson = null;
            exampleJson = "{\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"status\" : 6\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Program>(exampleJson)
                        : default(Program);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 添加企业成员进项目
        /// </summary>
        /// <remarks>添加企业成员进项目</remarks>
        /// <param name="body"></param>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/members")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdProgramsProgramIdMembers")]
        [SwaggerResponse(statusCode: 201, type: typeof(Member), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdProgramsProgramIdMembers([FromBody]Body11 body, [FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(Member));
            string exampleJson = null;
            exampleJson = "{\n  \"enterprise_role\" : {\n    \"ident\" : \"ident\",\n    \"name\" : \"name\",\n    \"description\" : \"description\",\n    \"id\" : 5,\n    \"is_system_default\" : true,\n    \"is_default\" : true\n  },\n  \"occupation\" : \"occupation\",\n  \"is_block\" : true,\n  \"remark\" : \"remark\",\n  \"block_message\" : \"block_message\",\n  \"enterprise_version\" : 5,\n  \"is_guided\" : true,\n  \"pinyin\" : \"pinyin\",\n  \"phone\" : \"phone\",\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_feedback\" : true,\n  \"user\" : {\n    \"user_color\" : 1,\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"id\" : 6,\n    \"state\" : \"state\",\n    \"username\" : \"username\"\n  },\n  \"email\" : \"email\",\n  \"username\" : \"username\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Member>(exampleJson)
                        : default(Member);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 项目关联仓库
        /// </summary>
        /// <remarks>项目关联仓库</remarks>
        /// <param name="body"></param>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <response code="201">项目关联仓库</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}/projects")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdProgramsProgramIdProjects")]
        [SwaggerResponse(statusCode: 201, type: typeof(Project), description: "项目关联仓库")]
        public virtual IActionResult PostEnterpriseIdProgramsProgramIdProjects([FromBody]Body12 body, [FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(Project));
            string exampleJson = null;
            exampleJson = "{\n  \"path\" : \"path\",\n  \"public\" : 5,\n  \"name\" : \"name\",\n  \"id\" : 1,\n  \"enterprise_id\" : 5\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Project>(exampleJson)
                        : default(Project);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 更新项目
        /// </summary>
        /// <remarks>更新项目</remarks>
        /// <param name="body"></param>
        /// <param name="programId">项目 id</param>
        /// <param name="enterpriseId"></param>
        /// <response code="200">返回格式</response>
        [HttpPut]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/programs/{program_id}")]
        [ValidateModelState]
        [SwaggerOperation("PutEnterpriseIdProgramsProgramId")]
        [SwaggerResponse(statusCode: 200, type: typeof(Program), description: "返回格式")]
        public virtual IActionResult PutEnterpriseIdProgramsProgramId([FromBody]Body10 body, [FromRoute][Required]int? programId, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(Program));
            string exampleJson = null;
            exampleJson = "{\n  \"ident\" : \"ident\",\n  \"name\" : \"name\",\n  \"outsourced\" : true,\n  \"description\" : \"description\",\n  \"id\" : 0,\n  \"type\" : \"type\",\n  \"status\" : 6\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Program>(exampleJson)
                        : default(Program);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
