/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class ProjectTagsApiController : ControllerBase
    { 
        /// <summary>
        /// 删除标签
        /// </summary>
        /// <remarks>删除标签</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="name">标签名称</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <response code="204">删除标签</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/tags/destroy")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProjectsProjectIdTagsDestroy")]
        public virtual IActionResult DeleteEnterpriseIdProjectsProjectIdTagsDestroy([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery][Required()]string name, [FromQuery]string accessToken, [FromQuery]string qt)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取仓库的标签列表
        /// </summary>
        /// <remarks>获取仓库的标签列表</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <param name="search">搜索关键字</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        /// <response code="404">没有相关数据</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/tags")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProjectsProjectIdTags")]
        [SwaggerResponse(statusCode: 200, type: typeof(Tag), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProjectsProjectIdTags([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery]string accessToken, [FromQuery]string qt, [FromQuery]string search, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(Tag));

            //TODO: Uncomment the next line to return response 404 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(404);
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"commit\" : { },\n  \"operating\" : { },\n  \"message\" : \"message\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Tag>(exampleJson)
                        : default(Tag);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 新建标签
        /// </summary>
        /// <remarks>新建标签</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/tags")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdProjectsProjectIdTags")]
        [SwaggerResponse(statusCode: 201, type: typeof(Tag), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdProjectsProjectIdTags([FromBody]Body46 body, [FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(Tag));
            string exampleJson = null;
            exampleJson = "{\n  \"name\" : \"name\",\n  \"commit\" : { },\n  \"operating\" : { },\n  \"message\" : \"message\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Tag>(exampleJson)
                        : default(Tag);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
