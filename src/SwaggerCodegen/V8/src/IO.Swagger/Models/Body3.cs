/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body3 : IEquatable<Body3>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 加入ids, 逗号(,)隔开。add_ids，remove_ids 至少填一项，不能都为空
        /// </summary>
        /// <value>加入ids, 逗号(,)隔开。add_ids，remove_ids 至少填一项，不能都为空</value>
        [DataMember(Name="add_ids")]
        public string AddIds { get; set; }

        /// <summary>
        /// 退出ids, 逗号(,)隔开
        /// </summary>
        /// <value>退出ids, 逗号(,)隔开</value>
        [DataMember(Name="remove_ids")]
        public string RemoveIds { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body3 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  AddIds: ").Append(AddIds).Append("\n");
            sb.Append("  RemoveIds: ").Append(RemoveIds).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body3)obj);
        }

        /// <summary>
        /// Returns true if Body3 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body3 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body3 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    AddIds == other.AddIds ||
                    AddIds != null &&
                    AddIds.Equals(other.AddIds)
                ) && 
                (
                    RemoveIds == other.RemoveIds ||
                    RemoveIds != null &&
                    RemoveIds.Equals(other.RemoveIds)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (AddIds != null)
                    hashCode = hashCode * 59 + AddIds.GetHashCode();
                    if (RemoveIds != null)
                    hashCode = hashCode * 59 + RemoveIds.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body3 left, Body3 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body3 left, Body3 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
