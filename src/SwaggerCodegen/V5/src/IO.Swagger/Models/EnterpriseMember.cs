/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 修改企业成员权限或备注
    /// </summary>
    [DataContract]
    public partial class EnterpriseMember : IEquatable<EnterpriseMember>
    { 
        /// <summary>
        /// Gets or Sets Url
        /// </summary>
        [DataMember(Name="url")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or Sets Active
        /// </summary>
        [DataMember(Name="active")]
        public string Active { get; set; }

        /// <summary>
        /// Gets or Sets Remark
        /// </summary>
        [DataMember(Name="remark")]
        public string Remark { get; set; }

        /// <summary>
        /// Gets or Sets Role
        /// </summary>
        [DataMember(Name="role")]
        public string Role { get; set; }

        /// <summary>
        /// Gets or Sets Outsourced
        /// </summary>
        [DataMember(Name="outsourced")]
        public string Outsourced { get; set; }

        /// <summary>
        /// Gets or Sets Enterprise
        /// </summary>
        [DataMember(Name="enterprise")]
        public EnterpriseBasic Enterprise { get; set; }

        /// <summary>
        /// Gets or Sets User
        /// </summary>
        [DataMember(Name="user")]
        public string User { get; set; }

        /// <summary>
        /// Gets or Sets LastEventAt
        /// </summary>
        [DataMember(Name="last_event_at")]
        public string LastEventAt { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EnterpriseMember {\n");
            sb.Append("  Url: ").Append(Url).Append("\n");
            sb.Append("  Active: ").Append(Active).Append("\n");
            sb.Append("  Remark: ").Append(Remark).Append("\n");
            sb.Append("  Role: ").Append(Role).Append("\n");
            sb.Append("  Outsourced: ").Append(Outsourced).Append("\n");
            sb.Append("  Enterprise: ").Append(Enterprise).Append("\n");
            sb.Append("  User: ").Append(User).Append("\n");
            sb.Append("  LastEventAt: ").Append(LastEventAt).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((EnterpriseMember)obj);
        }

        /// <summary>
        /// Returns true if EnterpriseMember instances are equal
        /// </summary>
        /// <param name="other">Instance of EnterpriseMember to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(EnterpriseMember other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Url == other.Url ||
                    Url != null &&
                    Url.Equals(other.Url)
                ) && 
                (
                    Active == other.Active ||
                    Active != null &&
                    Active.Equals(other.Active)
                ) && 
                (
                    Remark == other.Remark ||
                    Remark != null &&
                    Remark.Equals(other.Remark)
                ) && 
                (
                    Role == other.Role ||
                    Role != null &&
                    Role.Equals(other.Role)
                ) && 
                (
                    Outsourced == other.Outsourced ||
                    Outsourced != null &&
                    Outsourced.Equals(other.Outsourced)
                ) && 
                (
                    Enterprise == other.Enterprise ||
                    Enterprise != null &&
                    Enterprise.Equals(other.Enterprise)
                ) && 
                (
                    User == other.User ||
                    User != null &&
                    User.Equals(other.User)
                ) && 
                (
                    LastEventAt == other.LastEventAt ||
                    LastEventAt != null &&
                    LastEventAt.Equals(other.LastEventAt)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Url != null)
                    hashCode = hashCode * 59 + Url.GetHashCode();
                    if (Active != null)
                    hashCode = hashCode * 59 + Active.GetHashCode();
                    if (Remark != null)
                    hashCode = hashCode * 59 + Remark.GetHashCode();
                    if (Role != null)
                    hashCode = hashCode * 59 + Role.GetHashCode();
                    if (Outsourced != null)
                    hashCode = hashCode * 59 + Outsourced.GetHashCode();
                    if (Enterprise != null)
                    hashCode = hashCode * 59 + Enterprise.GetHashCode();
                    if (User != null)
                    hashCode = hashCode * 59 + User.GetHashCode();
                    if (LastEventAt != null)
                    hashCode = hashCode * 59 + LastEventAt.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(EnterpriseMember left, EnterpriseMember right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EnterpriseMember left, EnterpriseMember right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
