/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body23 : IEquatable<Body23>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 起点名称, 默认：master
        /// </summary>
        /// <value>起点名称, 默认：master</value>
        [Required]
        [DataMember(Name="refs")]
        public string Refs { get; set; }

        /// <summary>
        /// 新创建的标签名称
        /// </summary>
        /// <value>新创建的标签名称</value>
        [Required]
        [DataMember(Name="tag_name")]
        public string TagName { get; set; }

        /// <summary>
        /// Tag 描述, 默认为空
        /// </summary>
        /// <value>Tag 描述, 默认为空</value>
        [DataMember(Name="tag_message")]
        public string TagMessage { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body23 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  Refs: ").Append(Refs).Append("\n");
            sb.Append("  TagName: ").Append(TagName).Append("\n");
            sb.Append("  TagMessage: ").Append(TagMessage).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body23)obj);
        }

        /// <summary>
        /// Returns true if Body23 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body23 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body23 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    Refs == other.Refs ||
                    Refs != null &&
                    Refs.Equals(other.Refs)
                ) && 
                (
                    TagName == other.TagName ||
                    TagName != null &&
                    TagName.Equals(other.TagName)
                ) && 
                (
                    TagMessage == other.TagMessage ||
                    TagMessage != null &&
                    TagMessage.Equals(other.TagMessage)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (Refs != null)
                    hashCode = hashCode * 59 + Refs.GetHashCode();
                    if (TagName != null)
                    hashCode = hashCode * 59 + TagName.GetHashCode();
                    if (TagMessage != null)
                    hashCode = hashCode * 59 + TagMessage.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body23 left, Body23 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body23 left, Body23 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
